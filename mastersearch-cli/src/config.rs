use serde::Deserialize;
use std::{
    collections::HashMap,
    env,
    fs::{self, OpenOptions},
    io::{Read, Write},
    path::PathBuf,
};

use crate::error::ConfigError;

const DEFAULT_CONFIG: &str = r#"[profiles]
default = ["Wikipedia", "Stackoverflow"]

[providers]
[providers.Wikipedia]
id = "wikimedia"
[providers.Wikipedia.config]
url = "https://en.wikipedia.org"
limit = "5"

[providers.Stackoverflow]
id = "stackexchange"
[providers.Stackoverflow.config]
site = "stackoverflow"
limit = "5"
"#;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub profiles: HashMap<String, Vec<String>>,
    pub providers: HashMap<String, Provider>,
}

#[derive(Deserialize, Debug)]
pub struct Provider {
    pub id: String,
    pub config: HashMap<String, String>,
}

impl Config {
    pub fn new() -> Result<Self, ConfigError> {
        let conf_file = PathBuf::from(
            env::var("MS_CONFIG_FILE").unwrap_or(
                env::var("XDG_CONFIG_HOME")
                    .map(|s| s + "/mastersearch/config.toml")
                    .unwrap_or(
                        env::var("HOME")
                            .map(|s| s + "/.config/mastersearch/config.toml")
                            .map_err(|_| ConfigError::NoConfigPath)?,
                    ),
            ),
        );

        log::debug!("Reading configuration at {}", conf_file.to_string_lossy());

        if !conf_file.exists() {
            log::debug!("Configuration does not yet exist. Overwriting with default");
            let mut conf_dir = conf_file.clone();
            conf_dir.pop();
            fs::create_dir_all(conf_dir)?;

            let mut file = OpenOptions::new()
                .write(true)
                .create(true)
                .open(conf_file)?;
            let _ = write!(file, "{}", DEFAULT_CONFIG);
            let config: Config = toml::from_str(DEFAULT_CONFIG)?;
            Ok(config)
        } else {
            let mut file = OpenOptions::new().read(true).open(conf_file)?;
            let mut content = String::new();
            file.read_to_string(&mut content)?;
            let config: Config = toml::from_str(&content)?;
            Ok(config)
        }
    }
}
