use std::fmt::Display;

#[derive(Debug)]
pub enum ConfigError {
    NoConfigPath,
    FailedParse(toml::de::Error),
    FailedIO(std::io::Error),
}

impl Display for ConfigError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NoConfigPath => write!(f, "No configuration path could be found"),
            Self::FailedParse(e) => write!(f, "Failed to parse configuration: {}", e),
            Self::FailedIO(e) => write!(f, "IO Exception: {}", e),
        }
    }
}

impl std::error::Error for ConfigError {}

impl From<toml::de::Error> for ConfigError {
    fn from(e: toml::de::Error) -> Self {
        Self::FailedParse(e)
    }
}

impl From<std::io::Error> for ConfigError {
    fn from(e: std::io::Error) -> Self {
        Self::FailedIO(e)
    }
}

#[derive(Debug)]
pub enum ApplicationError {
    NoProviderWithId(String),
    NoProfileWithName(String),
    FailedCreateProviderFromConfig(String, ms::ProviderCreationError),
}

impl Display for ApplicationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NoProviderWithId(id) => write!(f, "No provider with id '{}' can be found.", id),
            Self::NoProfileWithName(name) => {
                write!(f, "No profile with name '{} can be found'", name)
            }
            Self::FailedCreateProviderFromConfig(name, e) => write!(
                f,
                "Failed to create provider with name '{}' from configuration: {}",
                name, e
            ),
        }
    }
}

impl std::error::Error for ApplicationError {}
