mod config;
mod error;

use std::{io::BufRead, process::Command};

use ms::{MasterConfig, Profile, SearchProviderResolver, SearchQuery, Searcher};
use ms_all::all_creators;

use crate::{config::Config, error::ApplicationError};

const PROFILE_CHAR: char = ':';
const DEFAULT_OPEN: &str = "xdg-open";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let config = Config::new()?;
    log::debug!("Configuration: {:?}", config);

    let masterconfig = MasterConfig::default();
    let creator_resolver = all_creators(masterconfig);

    let mut providers = SearchProviderResolver::default();

    for (name, provider) in config.providers {
        let creator = creator_resolver
            .get(provider.id.clone())
            .ok_or(ApplicationError::NoProviderWithId(provider.id))?;
        let provider = creator
            .create(&provider.config)
            .map_err(|e| ApplicationError::FailedCreateProviderFromConfig(name.clone(), e))?;
        providers.insert(name, provider);
    }

    let searcher = Searcher::new(providers);

    let args = std::env::args().skip(1).collect::<Vec<String>>().join(" ");
    let (profile_name, search) = if args.starts_with(PROFILE_CHAR) {
        let (profile_with_bang, search) = args.split_once(' ').unwrap_or((&args, ""));
        let profile = profile_with_bang
            .strip_prefix(PROFILE_CHAR)
            .unwrap_or_else(|| panic!("Profile to start with {}", PROFILE_CHAR));
        (profile, search)
    } else {
        ("default", &args[..])
    };
    let query = SearchQuery(search.to_string());
    let profile = Profile {
        providers: config
            .profiles
            .get(profile_name)
            .ok_or_else(|| ApplicationError::NoProfileWithName(profile_name.to_string()))?
            .clone(),
    };

    let result = searcher.search(&query, profile).await;

    let mut counter = 0;
    let mut results = vec![];
    for (name, res) in result {
        println!("# {}", name);
        if let Ok(res) = res {
            for search in res {
                println!("{}: {}", counter, search.title);
                if let Some(description) = &search.description {
                    println!("\t{}", description);
                }
                counter += 1;
                results.push(search);
            }
        } else {
            println!("Error: {}", res.err().expect("Result to be a error"));
        }
        println!();
    }

    let stdin = std::io::stdin();
    let mut lines = stdin.lock().lines();

    println!("Open: ");

    let open_line = lines.next().expect("Failed to read line")?;
    let open = open_line.parse::<usize>().expect("Failed to parse integer");
    println!("");

    if open > counter {
        panic!("Index out of bounds")
    }

    let to_open = &results[open];
    let command = to_open
        .open_with
        .as_ref()
        .map(|s| &s[..])
        .unwrap_or(DEFAULT_OPEN);
    let mut _child = Command::new(command)
        .arg(&to_open.url)
        .stdout(to_open.stdio())
        .stdin(to_open.stdio())
        .spawn()
        .expect("Failed to spawn child");
    let _exit = _child.wait().expect("Failed to wait for child");
    log::debug!("Opened {} with {}", to_open.url, command);
    println!("Opened");

    Ok(())
}
