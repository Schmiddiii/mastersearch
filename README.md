# Mastersearch

Searching the web from the CLI and dmenu.

Mastersearch combines the search functionality of some popular websites into one customizable, easy to use search tool for the command line or dmenu.

## Supported Websites

- [Wikimedia](https://wikimediafoundation.org/) for [Wikipedia](https://wikipedia.org) in all available languages (id: `wikimedia`).
- [StackExchange](https://stackexchange.com/) for [Stackoverflow](https://stackoverflow.com) and similar websites (id: `stackexchange`).
- [crates.io](https://crates.io) (id: `crates.io`).
- [github.com](https://github.com) (id: `github`)
- [DuckDuckGo](https://duckduckgo.com) (id: `duckduckgo`)
- apropos (the linux command, not really a website) (id: `apropos`)
- [AUR](https://aur.archlinux.org/) (id: `aur`)

For more details, please see the README of the respective module.

## Planned Websites

- [gitlab.com](https://gitlab.com) (Search only indexes projects from paid customers, so not really usefull)

If you want to see other websites supported, please create a issue or consider working on it yourself and open a PR.

## Usage

Build `mastersearch-cli` (`cargo build --release -p mastersearch-cli`, this neads a pretty new Rust version) and install the resulting binary from `target/release/mastersearch-cli` somewhere in your `$PATH`. You should also consider creating an alias. You can also do the same for `mastersearch-dmenu`.

Now just type `mastersearch-cli <Your Search Goes Here>` to search. By default, [en.wikipedia.org](https://en.wikipedia.org) and [stackoverflow.com](https://stackoverflow.com) will be searched. You can change your searchers the settings.  For more information, see the `Settings`-section.

To search from another profile than the default one, use `mastersearch-cli :<profile> <Your Search Goes Here>`.

## Settings

The settings can be found in `~/.config/mastersearch/config.toml`, which will be created on first execution of the program. You can also change the location of the settings using `$MS_CONFIG_FILE`.

The settings are split up into two parts, the profiles and the providers. An additional third part `settings` may be given when running `mastersearch-dmenu`, `mastersearch-cli` will ignore this part.

### Profiles

The profiles describe where to search. Every profile is a array of string, where each string references a name in the providers section. At least the `default`-profile should be given, this will be used when no profile for a search is given.

A `profiles`-section might look like this:

```toml
[profiles]
default = ["Wikipedia", "Stackoverflow"]
rust = ["CratesIO"]
```

Here, `default` and `rust` are the profile names, `Wikipedia`, `Stackoverflow` and `CratesIO` are the names referencing providers.

### Providers

The providers are used for searching. Every provider has a name, given as the key of the provider, a id and (depending on the id) specific configuration.

A `providers`-section might look like this:

```toml
[providers]
[providers.Wikipedia]
id = "wikimedia"
[providers.Wikipedia.config]
url = "https://en.wikipedia.org"
limit = "3"

[providers.Stackoverflow]
id = "stackexchange"
[providers.Stackoverflow.config]
site = "stackoverflow"
limit = "5"

[providers.CratesIO]
id = "crates.io"
[providers.CratesIO.config]
show-description = "false"
```

Here `Wikipedia`, `Stackoverflow` and `CratesIO` are the names, `wikimedia`, `stackexchange` and `crates.io` are the ids and the keys in `config` are provider-specific configuration. Please note that every value in the `config`-section must be a string. Most providers also support the configuration options `limit` (default: infinite) and `show-description` (default: `true`).

### Settings

These settings are `mastersearch-dmenu`-specific and specify how to open results in the terminal (e.g. `apropos`). A example might be:

```toml
[config]
terminal = "alacritty"
terminal-open = "-e"
```

The given values from the example also represent the default values if nothing is given.

## Creating support for a website

For information on how to support the search for a application, look at the README located in `mastersearch`.
