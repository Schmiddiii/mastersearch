mod api;

use std::collections::HashMap;

use api::Aur;
use async_trait::async_trait;
use ms::{
    MasterConfig, ProviderCreationError, SearchCreator, SearchProvider, SearchProviderWrapper,
    SearchResult,
};
use rrw::RestConfigBuilder;

pub struct AurProvider {
    aur: Aur,
}

pub struct AurCreator {
    pub client: reqwest::Client,
}

impl From<MasterConfig> for AurCreator {
    fn from(c: MasterConfig) -> Self {
        Self { client: c.client }
    }
}

impl SearchCreator for AurCreator {
    fn id(&self) -> &'static str {
        "aur"
    }

    fn create(
        &self,
        config: &HashMap<String, String>,
    ) -> Result<Box<dyn SearchProvider>, ProviderCreationError> {
        Ok(Box::new(SearchProviderWrapper::wrap(
            AurProvider {
                aur: Aur::new(
                    RestConfigBuilder::new("https://aur.archlinux.org/rpc/")
                        .client(self.client.clone())
                        .build(),
                ),
            },
            config,
        )))
    }
}

#[async_trait(?Send)]
impl SearchProvider for AurProvider {
    async fn search(
        &self,
        query: &ms::SearchQuery,
    ) -> Result<Vec<ms::SearchResult>, ms::SearchError> {
        Ok(self
            .aur
            .search(&query.0)
            .await
            .map_err(rrw_to_ms_error)?
            .results
            .into_iter()
            .map(|r| SearchResult {
                url: format!("https://aur.archlinux.org/packages/{}", &r.name),
                title: r.name,
                description: Some(r.description),
                ..Default::default()
            })
            .collect())
    }
}

fn rrw_to_ms_error<T>(e: rrw::Error<T>) -> ms::SearchError {
    match e {
        rrw::Error::Request(e) => ms::SearchError::Request(e),
        _ => ms::SearchError::Other,
    }
}
