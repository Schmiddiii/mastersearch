mod structure;
use rrw::{Error, RestRequest, StandardRestError};
pub(crate) use structure::*;

#[derive(Clone)]
pub struct Aur {
    _rest: rrw::RestConfig,
}

impl Aur {
    pub fn new(rest: rrw::RestConfig) -> Self {
        Self { _rest: rest }
    }
}

impl Aur {
    pub async fn search<S: AsRef<str>>(
        &self,
        query: S,
    ) -> std::result::Result<structure::Results, Error<StandardRestError>> {
        let query = Query {
            v: "5".to_string(),
            _type: "search".to_string(),
            arg: query.as_ref().to_string(),
        };
        let request = RestRequest::<&Query, ()>::get("").query(&query);

        Ok(self._rest.execute(&request).await?)
    }
}
