use serde::{Deserialize, Serialize};

#[derive(Serialize, Default, Clone)]
pub struct Query {
    pub v: String,
    #[serde(rename = "type")]
    pub _type: String,
    pub arg: String,
}

#[derive(Deserialize, Default, Clone)]
pub struct Results {
    pub results: Vec<Result>,
}

#[derive(Deserialize, Default, Clone)]
pub struct Result {
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Description")]
    pub description: String,
}
