use std::collections::HashMap;

use mastersearch_aur::AurCreator;
use ms::{SearchCreator, SearchQuery};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = reqwest::ClientBuilder::new()
        .gzip(true)
        .user_agent("Mastersearch-AUR/0.1.0 (mastersearch-aur@schmiddi.anonaddy.com) reqwest/0.11")
        .build()
        .expect("Failed to build reqwest::Client");
    let config = HashMap::new();
    let provider = AurCreator { client }
        .create(&config)
        .expect("Failed to build AurProvider");
    for result in provider
        .search(&SearchQuery("foobar".to_string()))
        .await?
    {
        println!("Result: {} ({})", result.title, result.url);
    }
    Ok(())
}
