# Mastersearch-AUR (ID: `aur`)

The mastersearch interface to the [AUR](https://aur.archlinux.org/) (Arch User Repository).

## Configuration

The AUR-interface has no provider-specific configuration.
