# Mastersearch-Apropos (ID: `apropos`)

The mastersearch interface to `apropos`, the linux command. This command will be run locally and will therefore need to be installed locally. Additionally, the commands `whereis` and `man` will be needed, but are probably already installed.
For better results, `apropos` will be used with the `-e` flag. This may change or be configurable in the future.

## Configuration

This provider has no provider-specific configuration options.

Please remember that this will not open results in your browser and will use the terminal instead. When running `mastersearch-cli`, the current terminal will be used. With `mastersearch-dmenu`, a new terminal will be spawned based on your configuration. For more information, look at the README in the root repository.
