use std::{
    collections::HashMap,
    process::{Command, Stdio},
};

use async_trait::async_trait;
use ms::{
    MasterConfig, ProviderCreationError, SearchCreator, SearchProvider, SearchProviderWrapper,
};

const APROPOS: &str = "apropos";
const MAN: &str = "man";
const WHEREIS: &str = "whereis";

pub struct AproposCreator {}

impl From<MasterConfig> for AproposCreator {
    fn from(_: MasterConfig) -> Self {
        Self {}
    }
}

pub struct AproposProvider {}

impl SearchCreator for AproposCreator {
    fn id(&self) -> &'static str {
        "apropos"
    }

    fn create(
        &self,
        config: &HashMap<String, String>,
    ) -> Result<Box<dyn SearchProvider>, ProviderCreationError> {
        Ok(Box::new(SearchProviderWrapper::wrap(
            AproposProvider {},
            config,
        )))
    }
}

#[async_trait(?Send)]
impl SearchProvider for AproposProvider {
    async fn search(
        &self,
        query: &ms::SearchQuery,
    ) -> Result<Vec<ms::SearchResult>, ms::SearchError> {
        let output = Command::new(APROPOS)
            .arg("-e")
            .arg(&query.0)
            .stdin(Stdio::null())
            .stdout(Stdio::piped())
            .output()
            .map(|o| o.stdout)
            .unwrap_or_default();
        Ok(String::from_utf8_lossy(&output)
            .lines()
            .map(|s| s.split_once('-').unwrap_or((s, "")))
            .map(|(n, d)| (n.trim().to_owned(), d.trim().to_owned()))
            .map(|(n, d)| {
                let name_no_page = n.split(' ').next().unwrap_or(&n);
                let whereis = Command::new(WHEREIS)
                    .args(&["-m", &name_no_page])
                    .output()
                    .map(|o| o.stdout)
                    .unwrap_or_default();
                let path = String::from_utf8_lossy(&whereis)
                    .rsplit(":")
                    .next()
                    .unwrap_or_default()
                    .to_owned();
                ms::SearchResult {
                    title: n,
                    url: path.trim().to_owned(),
                    description: Some(d),
                    open_with: Some(MAN.to_string()),
                    inherit_stdio: true,
                    needs_terminal: true,
                }
            })
            .collect())
    }
}
