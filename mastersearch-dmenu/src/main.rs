mod config;
mod error;

use std::{
    io::Write,
    process::{Command, Stdio},
};

use ms::{MasterConfig, Profile, SearchProviderResolver, SearchQuery, Searcher};
use ms_all::all_creators;

use crate::{config::Config, error::ApplicationError};

const PROFILE_CHAR: char = ':';
const DEFAULT_OPEN: &str = "xdg-open";
const TERMINAL: &str = "alacritty";
const TERMINAL_OPEN: &str = "-e";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let config = Config::new()?;
    log::debug!("Configuration: {:?}", config);

    let masterconfig = MasterConfig::default();
    let creator_resolver = all_creators(masterconfig);

    let mut providers = SearchProviderResolver::default();

    for (name, provider) in config.providers {
        let creator = creator_resolver
            .get(provider.id.clone())
            .ok_or(ApplicationError::NoProviderWithId(provider.id))?;
        let provider = creator
            .create(&provider.config)
            .map_err(|e| ApplicationError::FailedCreateProviderFromConfig(name.clone(), e))?;
        providers.insert(name, provider);
    }

    let searcher = Searcher::new(providers);

    let mut dmenu_search = Command::new("dmenu")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .args(["-p", "Search"])
        .spawn()
        .expect("Failed to run `dmenu`");

    {
        let mut stdin = dmenu_search.stdin.take().expect("Failed to open stdin");
        stdin
            .write_all("".as_bytes())
            .expect("Failed to write to stdin");
    }

    let output_bytes = dmenu_search
        .wait_with_output()
        .expect("Failed to read stdout")
        .stdout;
    let output = String::from_utf8_lossy(&output_bytes);

    let (profile_name, search) = if output.starts_with(PROFILE_CHAR) {
        let (profile_with_bang, search) = output.split_once(' ').unwrap_or((&output, ""));
        let profile = profile_with_bang
            .strip_prefix(PROFILE_CHAR)
            .unwrap_or_else(|| panic!("Profile to start with {}", PROFILE_CHAR));
        (profile, search.trim())
    } else {
        ("default", output.trim())
    };
    let query = SearchQuery(search.to_string());
    let profile = Profile {
        providers: config
            .profiles
            .get(profile_name)
            .ok_or_else(|| ApplicationError::NoProfileWithName(profile_name.to_string()))?
            .clone(),
    };

    let result = searcher.search(&query, profile).await;

    let mut dmenu_input = "".to_string();
    let mut counter = 0;
    let mut results = vec![];
    for (name, res) in result {
        dmenu_input += &format!("# {}\n", name);
        if let Ok(res) = res {
            for search in res {
                dmenu_input += &format!("{}: {}\n", counter, search.title);
                if let Some(description) = &search.description {
                    dmenu_input += &format!("\t{}\n", description);
                }
                counter += 1;
                results.push(search);
            }
        } else {
            dmenu_input += &format!("Error: {}", res.err().expect("Result to be a error"));
        }
        dmenu_input += "\n";
    }

    let mut dmenu_open = Command::new("dmenu")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .args(["-p", "Open", "-l", "10"])
        .spawn()
        .expect("Failed to run `dmenu`");

    {
        let mut stdin = dmenu_open.stdin.take().expect("Failed to open stdin");
        stdin
            .write_all(dmenu_input.as_bytes())
            .expect("Failed to write to stdin");
    }

    let output_open_bytes = dmenu_open
        .wait_with_output()
        .expect("Failed to read stdout")
        .stdout;
    let output_open = String::from_utf8_lossy(&output_open_bytes);

    let open = output_open
        .split(':')
        .next()
        .map(|s| s.parse::<usize>().expect("Failed to parse integer"))
        .expect("Failed to parse integer");

    if open > counter {
        panic!("Index out of bounds")
    }

    let to_open = &results[open];
    let (command, args) = if to_open.needs_terminal {
        (
            config
                .config
                .as_ref()
                .map(|c| &c.terminal[..])
                .unwrap_or(TERMINAL),
            vec![
                config
                    .config
                    .as_ref()
                    .map(|c| &c.terminal_open[..])
                    .unwrap_or(TERMINAL_OPEN),
                &to_open
                    .open_with
                    .as_ref()
                    .map(|s| &s[..])
                    .unwrap_or(DEFAULT_OPEN),
                &to_open.url[..],
            ],
        )
    } else {
        (
            to_open
                .open_with
                .as_ref()
                .map(|s| &s[..])
                .unwrap_or(DEFAULT_OPEN),
            vec![&to_open.url[..]],
        )
    };
    log::debug!(
        "Opened {} with command {}, args {:?}",
        to_open.url,
        command,
        args
    );
    let _child = Command::new(&command)
        .args(&args)
        .stdout(Stdio::null())
        .spawn();

    Ok(())
}
