# Mastersearch-StackExchange (ID: `stackexchange`)

The mastersearch interface to StackExchange websites like [StackOverflow](https://stackoverflow.com).

## Configuration

This provider has one configuration option. The `site`-key describes what website of the StackExchange-ecosystem to query. See [https://stackexchange.com/sites](https://stackexchange.com/sites) for all availble sites. To find out the exact name, you can use the `api-site-parameter` field of the [site-API](https://api.stackexchange.com/docs/sites#filter=default&run=true). One example is `stackoverflow`.
