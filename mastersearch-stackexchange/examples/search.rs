use std::collections::HashMap;

use mastersearch_stackexchange::StackExchangeCreator;
use ms::{SearchCreator, SearchQuery};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let client = reqwest::ClientBuilder::new()
        .gzip(true)
        .build()
        .expect("Failed to build reqwest::Client");
    let mut config = HashMap::new();
    config.insert("site".to_string(), "stackoverflow".to_string());
    let provider = StackExchangeCreator { client }
        .create(&config)
        .expect("Failed to create StackExchangeProvider");
    for result in provider.search(&SearchQuery("hello".to_string())).await? {
        println!("Result: {} ({})", result.title, result.url);
    }
    Ok(())
}
