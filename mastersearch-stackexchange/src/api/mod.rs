mod structure;
use rrw::{Error, RestRequest, StandardRestError};
use rrw_macro::rest;
pub(crate) use structure::*;

#[rest]
impl StackExchange {
    pub async fn search(
        &self,
        query: &SearchQuery,
    ) -> std::result::Result<structure::Result, Error<StandardRestError>> {
        RestRequest::<&SearchQuery, ()>::get("/search").query(query)
    }
}
