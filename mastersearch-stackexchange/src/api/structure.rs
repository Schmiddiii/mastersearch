use serde::{Deserialize, Serialize};

#[derive(Serialize, Default, Clone)]
pub struct SearchQuery {
    pub site: String,
    pub intitle: String,
}

#[derive(Deserialize, Default, Clone)]
pub struct Result {
    pub items: Vec<Item>,
}

#[derive(Deserialize, Default, Clone)]
pub struct Item {
    pub link: String,
    pub title: String,
}
