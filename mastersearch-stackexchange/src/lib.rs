mod api;

use std::collections::HashMap;

use api::{SearchQuery, StackExchange};
use async_trait::async_trait;
use ms::{MasterConfig, ProviderCreationError, SearchCreator, SearchProvider, SearchResult, SearchProviderWrapper};
use rrw::RestConfigBuilder;

pub struct StackExchangeCreator {
    pub client: reqwest::Client,
}

impl From<MasterConfig> for StackExchangeCreator {
    fn from(c: MasterConfig) -> Self {
        Self { client: c.client }
    }
}

impl SearchCreator for StackExchangeCreator {
    fn id(&self) -> &'static str {
        "stackexchange"
    }

    fn create(
        &self,
        config: &HashMap<String, String>,
    ) -> Result<Box<dyn SearchProvider>, ProviderCreationError> {
        if let Some(site) = config.get("site") {
            Ok(Box::new(SearchProviderWrapper::wrap(StackExchangeProvider {
                site: site.to_string(),
                stack_exchange: StackExchange::new(
                    RestConfigBuilder::new("https://api.stackexchange.com")
                        .client(self.client.clone())
                        .build(),
                ),
            }, config)))
        } else {
            Err(ProviderCreationError { 
                id: self.id(),
                key: "site",
                value_hint: "A value for the key 'site' has to exist describing what API to query. See 'https://api.stackexchange.com/docs/sites#filter=default&run=true'."
            })
        }
    }
}

pub struct StackExchangeProvider {
    site: String,
    stack_exchange: StackExchange,
}

#[async_trait(?Send)]
impl SearchProvider for StackExchangeProvider {
    async fn search(
        &self,
        query: &ms::SearchQuery,
    ) -> Result<Vec<ms::SearchResult>, ms::SearchError> {
        Ok(self
            .stack_exchange
            .search(&SearchQuery {
                intitle: query.0.to_owned(),
                site: self.site.clone(),
            })
            .await
            .map_err(rrw_to_ms_error)?
            .items
            .into_iter()
            .map(|r| SearchResult {
                title: r.title,
                url: r.link,
                description: None,
                ..Default::default()
            })
            .collect())
    }
}

fn rrw_to_ms_error<T>(e: rrw::Error<T>) -> ms::SearchError {
    match e {
        rrw::Error::Request(e) => ms::SearchError::Request(e),
        _ => ms::SearchError::Other,
    }
}
