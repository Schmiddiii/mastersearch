# Mastersearch-Crates-IO (ID: `crates.io`)

The mastersearch interface to [crates.io](https://crates.io) to search for Rust crates.

## Configuration

There is one provider specific option, `doc-rs-links`. If this is set to `"true"`, the documentation for the crate in [docs.rs](https://docs.rs) will be opened instead of the [crates.io](https://crates.io) page.
