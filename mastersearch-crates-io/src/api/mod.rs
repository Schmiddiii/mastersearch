mod structure;
use rrw::{Error, RestRequest, StandardRestError};
pub(crate) use structure::*;

#[derive(Clone)]
pub struct CratesIO {
    _rest: rrw::RestConfig,
}

impl CratesIO {
    pub fn new(rest: rrw::RestConfig) -> Self {
        Self { _rest: rest }
    }
}

impl CratesIO {
    pub async fn search<S: AsRef<str>>(
        &self,
        query: S,
    ) -> std::result::Result<structure::Result, Error<StandardRestError>> {
        let query = Crates {
            q: query.as_ref().to_string(),
        };
        let request = RestRequest::<&Crates, ()>::get("/crates").query(&query);

        Ok(self._rest.execute(&request).await?)
    }
}
