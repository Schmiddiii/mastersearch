use serde::{Deserialize, Serialize};

#[derive(Serialize, Default, Clone)]
pub struct Crates {
    pub q: String,
}

#[derive(Deserialize, Default, Clone)]
pub struct Result {
    pub crates: Vec<Crate>,
}

#[derive(Deserialize, Default, Clone)]
pub struct Crate {
    pub id: String,
    pub name: String,
    pub description: Option<String>,
}
