mod api;

use std::collections::HashMap;

use api::CratesIO;
use async_trait::async_trait;
use ms::{
    MasterConfig, ProviderCreationError, SearchCreator, SearchProvider, SearchProviderWrapper,
    SearchResult,
};
use rrw::RestConfigBuilder;

pub struct CratesIOProvider {
    crates_io: CratesIO,
    doc_rs_links: bool,
}

pub struct CratesIOCreator {
    pub client: reqwest::Client,
}

impl From<MasterConfig> for CratesIOCreator {
    fn from(c: MasterConfig) -> Self {
        Self { client: c.client }
    }
}

impl SearchCreator for CratesIOCreator {
    fn id(&self) -> &'static str {
        "crates.io"
    }

    fn create(
        &self,
        config: &HashMap<String, String>,
    ) -> Result<Box<dyn SearchProvider>, ProviderCreationError> {
        Ok(Box::new(SearchProviderWrapper::wrap(
            CratesIOProvider {
                crates_io: CratesIO::new(
                    RestConfigBuilder::new("https://crates.io/api/v1")
                        .client(self.client.clone())
                        .build(),
                ),
                doc_rs_links: config
                    .get("doc-rs-links")
                    .map(|s| s == "true")
                    .unwrap_or(false),
            },
            config,
        )))
    }
}

#[async_trait(?Send)]
impl SearchProvider for CratesIOProvider {
    async fn search(
        &self,
        query: &ms::SearchQuery,
    ) -> Result<Vec<ms::SearchResult>, ms::SearchError> {
        Ok(self
            .crates_io
            .search(&query.0)
            .await
            .map_err(rrw_to_ms_error)?
            .crates
            .into_iter()
            .map(|r| SearchResult {
                title: r.name.clone(),
                url: if self.doc_rs_links {
                    format!("https://docs.rs/{}", r.name)
                } else {
                    format!("https://crates.io/crates/{}", r.id)
                },
                description: r.description,
                ..Default::default()
            })
            .collect())
    }
}

fn rrw_to_ms_error<T>(e: rrw::Error<T>) -> ms::SearchError {
    match e {
        rrw::Error::Request(e) => ms::SearchError::Request(e),
        _ => ms::SearchError::Other,
    }
}
