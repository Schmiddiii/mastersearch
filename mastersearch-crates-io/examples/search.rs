use std::collections::HashMap;

use mastersearch_crates_io::CratesIOCreator;
use ms::{SearchCreator, SearchQuery};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let client = reqwest::ClientBuilder::new()
        .user_agent("Mastersearch-Crates-io/0.1.0 (mastersearch-crates-io@schmiddi.anonaddy.com) reqwest/0.11")
        .build()
        .expect("Failed to build reqwest::Client");
    let config = HashMap::new();
    let provider = CratesIOCreator { client }
        .create(&config)
        .expect("Failed to build CratesIOProvider");
    for result in provider.search(&SearchQuery("search".to_string())).await? {
        println!("Result: {} ({})", result.title, result.url);
    }
    Ok(())
}
