mod api;

use std::collections::HashMap;

use api::{Github, SearchQuery};
use async_trait::async_trait;
use ms::{
    MasterConfig, ProviderCreationError, SearchCreator, SearchProvider, SearchProviderWrapper,
    SearchResult,
};
use rrw::RestConfigBuilder;

pub struct GithubCreator {
    pub client: reqwest::Client,
}

impl From<MasterConfig> for GithubCreator {
    fn from(c: MasterConfig) -> Self {
        Self { client: c.client }
    }
}

impl SearchCreator for GithubCreator {
    fn id(&self) -> &'static str {
        "github"
    }

    fn create(
        &self,
        config: &HashMap<String, String>,
    ) -> Result<Box<dyn SearchProvider>, ProviderCreationError> {
        Ok(Box::new(SearchProviderWrapper::wrap(
            GithubProvider {
                github: Github::new(
                    RestConfigBuilder::new("https://api.github.com")
                        .client(self.client.clone())
                        .build(),
                ),
            },
            config,
        )))
    }
}

pub struct GithubProvider {
    github: Github,
}

#[async_trait(?Send)]
impl SearchProvider for GithubProvider {
    async fn search(
        &self,
        query: &ms::SearchQuery,
    ) -> Result<Vec<ms::SearchResult>, ms::SearchError> {
        Ok(self
            .github
            .search_repositories(&SearchQuery {
                q: query.0.to_owned(),
            })
            .await
            .map_err(rrw_to_ms_error)?
            .items
            .into_iter()
            .map(|r| SearchResult {
                title: r.full_name,
                url: r.html_url,
                description: r.description,
                ..Default::default()
            })
            .collect())
    }
}

fn rrw_to_ms_error<T>(e: rrw::Error<T>) -> ms::SearchError {
    match e {
        rrw::Error::Request(e) => ms::SearchError::Request(e),
        _ => ms::SearchError::Other,
    }
}
