mod structure;
use rrw::{Error, RestRequest, StandardRestError};
use rrw_macro::rest;
pub(crate) use structure::*;

#[rest]
impl Github {
    pub async fn search_repositories(
        &self,
        query: &SearchQuery,
    ) -> std::result::Result<structure::RepositoryResult, Error<StandardRestError>> {
        RestRequest::<&SearchQuery, ()>::get("/search/repositories").query(query)
    }
}
