use serde::{Deserialize, Serialize};

#[derive(Serialize, Default, Clone)]
pub struct SearchQuery {
    pub q: String,
}

#[derive(Deserialize, Default, Clone)]
pub struct RepositoryResult {
    pub items: Vec<RepositoryItem>,
}

#[derive(Deserialize, Default, Clone)]
pub struct RepositoryItem {
    pub html_url: String,
    pub full_name: String,
    pub description: Option<String>,
}
