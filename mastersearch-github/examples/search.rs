use std::collections::HashMap;

use mastersearch_github::GithubCreator;
use ms::{SearchCreator, SearchQuery};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let client = reqwest::ClientBuilder::new()
        .gzip(true)
        .user_agent("Mastersearch/0.1.0 (mastersearch@schmiddi.anonaddy.com) reqwest/0.11")
        .build()
        .expect("Failed to build reqwest::Client");
    let config = HashMap::new();
    let provider = GithubCreator { client }
        .create(&config)
        .expect("Failed to create GithubProvider");
    for result in provider.search(&SearchQuery("hello".to_string())).await? {
        println!("Result: {} ({})", result.title, result.url);
    }
    Ok(())
}
