use ms::{MasterConfig, SearchCreatorResolver};

pub fn all_creators(config: MasterConfig) -> SearchCreatorResolver {
    let mut all = SearchCreatorResolver::default();
    #[cfg(feature = "stackexchange")]
    all.insert(ms_stackexchange::StackExchangeCreator::from(config.clone()));
    #[cfg(feature = "wikimedia")]
    all.insert(ms_wikimedia::WikimediaCreator::from(config.clone()));
    #[cfg(feature = "crates_io")]
    all.insert(ms_crates_io::CratesIOCreator::from(config.clone()));
    #[cfg(feature = "github")]
    all.insert(ms_github::GithubCreator::from(config.clone()));
    #[cfg(feature = "duckduckgo")]
    all.insert(ms_duckduckgo::DuckDuckGoCreator::from(config.clone()));
    #[cfg(feature = "apropos")]
    all.insert(ms_apropos::AproposCreator::from(config.clone()));
    #[cfg(feature = "aur")]
    all.insert(ms_aur::AurCreator::from(config));
    all
}
