use std::collections::HashMap;

use async_trait::async_trait;

use crate::{SearchError, SearchProvider, SearchQuery, SearchResult};

/// Wrap a [SearchProvider] to support configuration of:
///
/// - `limit`: Limit the amount of results.
/// - `show-description`: Hide the description if `false`.
pub struct SearchProviderWrapper<T> {
    provider: T,
    limit: Option<usize>,
    show_description: bool,
}

impl<T: SearchProvider> SearchProviderWrapper<T> {
    /// Wrap a [SearchProvider] with the configuration.
    pub fn wrap(provider: T, config: &HashMap<String, String>) -> Self {
        let limit = config.get("limit").map(|s| s.parse().ok()).flatten();
        let show_description = config
            .get("show-description")
            .map(|s| s.parse().ok())
            .flatten()
            .unwrap_or(true);
        Self {
            provider,
            limit,
            show_description,
        }
    }

    fn limit<I: 'static + Iterator<Item = SearchResult>>(
        &self,
        iter: I,
    ) -> Box<dyn Iterator<Item = SearchResult>> {
        if let Some(limit) = self.limit {
            Box::new(iter.take(limit))
        } else {
            Box::new(iter)
        }
    }

    fn descriptioned<I: 'static + Iterator<Item = SearchResult>>(
        &self,
        iter: I,
    ) -> Box<dyn Iterator<Item = SearchResult>> {
        if !self.show_description {
            Box::new(iter.map(|s| SearchResult {
                title: s.title,
                url: s.url,
                description: None,
                open_with: s.open_with,
                inherit_stdio: s.inherit_stdio,
                needs_terminal: s.needs_terminal,
            }))
        } else {
            Box::new(iter)
        }
    }
}

#[async_trait(?Send)]
impl<T: SearchProvider> SearchProvider for SearchProviderWrapper<T> {
    async fn search(&self, query: &SearchQuery) -> Result<Vec<SearchResult>, SearchError> {
        let res = self.provider.search(query).await?.into_iter();

        let limited = self.limit(res);
        let descriptioned = self.descriptioned(limited);

        Ok(descriptioned.collect())
    }
}
