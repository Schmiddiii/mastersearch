use std::collections::HashMap;

use async_trait::async_trait;

use crate::{SearchError, SearchQuery, SearchResult};

/// A implementation should resolve [SearchQueries][SearchQuery] into
/// [SearchResults][SearchResult].
#[async_trait(?Send)]
pub trait SearchProvider {
    async fn search(&self, query: &SearchQuery) -> Result<Vec<SearchResult>, SearchError>;
}

#[async_trait(?Send)]
impl SearchProvider for Box<dyn SearchProvider> {
    async fn search(&self, query: &SearchQuery) -> Result<Vec<SearchResult>, SearchError> {
        self.as_ref().search(query).await
    }
}

/// Resolve a [SearchProvider] from a name.
#[derive(Default)]
pub struct SearchProviderResolver {
    providers: HashMap<String, Box<dyn SearchProvider>>,
}

impl SearchProviderResolver {
    /// Insert a [SearchProvider] with a name.
    pub fn insert<S: AsRef<str>>(&mut self, name: S, provider: impl SearchProvider + 'static) {
        self.providers
            .insert(name.as_ref().to_owned(), Box::new(provider));
    }

    /// Try to get a [SearchProvider] from a name.
    pub fn get<S: AsRef<str>>(&self, name: S) -> Option<&dyn SearchProvider> {
        self.providers.get(&name.as_ref().to_owned()).map(|s| &**s)
    }
}
