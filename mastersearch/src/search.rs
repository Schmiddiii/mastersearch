use std::process::Stdio;

/// The query for a search.
pub struct SearchQuery(pub String);

/// The result of a search.
#[derive(Default)]
pub struct SearchResult {
    /// The title of the result.
    pub title: String,
    /// A url that can be navigated to showing details.
    pub url: String,
    /// A optional description.
    pub description: Option<String>,
    /// A optional command hint to open the url. If `None`, the url is assumed to be open in a
    /// default way.
    pub open_with: Option<String>,

    /// Whether Stdin and Stdout should be inherited from the main programm. Should not be overwritten in most cases,
    /// but will be needed when opening local files.
    pub inherit_stdio: bool,
    /// Whether the command needs to be run in a terminal.
    pub needs_terminal: bool,
}

impl SearchResult {
    pub fn stdio(&self) -> Stdio {
        if self.inherit_stdio {
            Stdio::inherit()
        } else {
            Stdio::null()
        }
    }
}
