use std::fmt::Display;

/// The search failed.
#[derive(Debug)]
pub enum SearchError {
    /// Requesting information failed.
    Request(reqwest::Error),
    /// Something else failed, e.g. parsing the result.
    ///
    /// TODO: Split this up more.
    Other,
}

impl Display for SearchError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SearchError::Request(e) => write!(f, "Failed to request: {}", e),
            SearchError::Other => write!(f, "Some error occured"),
        }
    }
}

impl std::error::Error for SearchError {}

impl From<reqwest::Error> for SearchError {
    fn from(e: reqwest::Error) -> Self {
        Self::Request(e)
    }
}

/// Failed to create the [SearchProvider][crate::SearchProvider].
#[derive(Debug)]
pub struct ProviderCreationError {
    /// The `id` of the [SearchCreator][crate::SearchCreator] that is misconfigured.
    pub id: &'static str,
    /// The `key` that is missing or could not be interpreted.
    pub key: &'static str,
    /// A hint what is wrong with the `key`.
    pub value_hint: &'static str,
}

impl Display for ProviderCreationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Failed to create search provider with id {}", self.id)?;
        write!(f, "Key: {}", self.key)?;
        write!(f, "Hint: {}", self.value_hint)
    }
}

impl std::error::Error for ProviderCreationError {}

/// A general error.
#[derive(Debug)]
pub enum Error {
    Search(SearchError),
    ProviderNotFound,
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Search(e) => write!(f, "Search failed: {}", e),
            Error::ProviderNotFound => write!(f, "Provider was not found"),
        }
    }
}
