/// A profile on what [SearchProviders][crate::SearchProvider] to search
pub struct Profile {
    /// The names of the [SearchProviders][crate::SearchProvider].
    pub providers: Vec<String>,
}
