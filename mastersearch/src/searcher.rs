use crate::{Error, Profile, SearchProviderResolver, SearchQuery, SearchResult};
use futures::stream;
use futures::StreamExt;

/// Execute a [SearchQuery] using a [Profile].
pub struct Searcher {
    resolver: SearchProviderResolver,
}

impl Searcher {
    /// Create a new [Searcher] from a [SearchProviderResolver].
    pub fn new(resolver: SearchProviderResolver) -> Self {
        Self { resolver }
    }

    /// Search a [SearchQuery], returning for each [SearchProvider][crate::SearchProvider] in the
    /// [Profile]:
    ///
    /// - The name of the provider from the search.
    /// - The result of the search.
    pub async fn search(
        &self,
        query: &SearchQuery,
        profile: Profile,
    ) -> Vec<(String, Result<Vec<SearchResult>, Error>)> {
        stream::iter(profile.providers)
            .then(|s| async move {
                let provider = self.resolver.get(&s);
                if let Some(provider) = provider {
                    (s, provider.search(query).await.map_err(Error::Search))
                } else {
                    (s, Err(Error::ProviderNotFound))
                }
            })
            .collect()
            .await
    }
}
