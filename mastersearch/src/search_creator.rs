use std::collections::HashMap;

use crate::{ProviderCreationError, SearchProvider};

/// Create a [SearchProvider].
pub trait SearchCreator {
    /// The id of the [SearchCreator].
    fn id(&self) -> &'static str;
    /// Create a [SearchProvider] from the given configuration
    ///
    /// If the configuration is good, one should also consider wrapping the custom [SearchProvider] with a [SearchProviderWrapper][crate::SearchProviderWrapper].
    /// If the configuration is false, a error should be returned describing the problem.
    fn create(
        &self,
        config: &HashMap<String, String>,
    ) -> Result<Box<dyn SearchProvider>, ProviderCreationError>;
}

/// Resolve a [SearchCreator] by its [id][SearchCreator::id].
#[derive(Default)]
pub struct SearchCreatorResolver {
    creators: HashMap<&'static str, Box<dyn SearchCreator>>,
}

impl SearchCreatorResolver {
    /// Insert a [SearchCreator].
    pub fn insert(&mut self, creator: impl SearchCreator + 'static) {
        self.creators.insert(creator.id(), Box::new(creator));
    }

    /// Try to get a [SearchCreator] from its [id][SearchCreator::id].
    pub fn get<S: AsRef<str>>(&self, name: S) -> Option<&dyn SearchCreator> {
        self.creators.get(name.as_ref()).map(|s| &**s)
    }
}
