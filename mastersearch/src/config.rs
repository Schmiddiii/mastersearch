/// The master configuration every [[crate::SearchCreator]] should be created from.
#[derive(Clone)]
pub struct MasterConfig {
    /// The [[reqwest::Client]] that should be used for every request.
    ///
    /// This supports `gzip` by default and has a custom user agent.
    pub client: reqwest::Client,
}

impl Default for MasterConfig {
    fn default() -> Self {
        Self {
            client: reqwest::ClientBuilder::new()
                .gzip(true)
                .user_agent("Mastersearch/0.1.0 (mastersearch@schmiddi.anonaddy.com) reqwest/0.11")
                .build()
                .expect("Failed to build reqwest::Client"),
        }
    }
}
