# Mastersearch

The library used to provide searching to the CLI.

## Adding support for a new website

It is pretty easy to add a new website that can be searched, as long as this website has a good, public API to do that.
Before programming an interface to your new website, you should also take a look at implementations of other websites to get a feel for what you need. I would recommend `mastersearch-stackexchange` for that.

To start off, you should create a new submodule with a sensible name, e.g. `mastersearch-my-website` (`cargo new --lib mastersearch-my-website`) and add it to the `Cargo.toml` in the root. Add the following dependencies to your new library:

```toml
[dependencies]
async-trait = "^0.1"
ms = { package = "mastersearch", path = "../mastersearch" }

# Optional but recommended if you call APIs.
reqwest = { version = "^0.11", features = ["gzip", "json"] }
rrw = "0.1.1"
rrw_macro = "0.1.1"
serde = { version = "1.0", features = ["derive"] }
```

The next step will be to create a interface to the website. I would recommend to use [rrw](https://gitlab.com/Schmiddiii/rrw) for a quick and easy interface. Note that you will not have to support every API endpoint, just the searching.

After that, implement a `SearchProvider` in your library. Again, use a sensible name for that, e.g. `MyWebsiteSearchProvider`. Take a look at the provided interface for that:

```rust
#[async_trait(?Send)]
pub trait SearchProvider {
    async fn search(&self, query: &SearchQuery) -> Result<Vec<SearchResult>, SearchError>;
}
```

You must only implement one async method, taking a reference to a `SearchQuery` (a wrapper around `String`) and returning your results, or a error. Note that you will have to add `#[async_trait(?Send)]` before your implementation as Rust does not allow async traits by default. To make the query, your `SearchProvider` should reference above created interface to the websites API.

The next step is to implement a `SearchCreator` that will create you `SearchProvider` from the configuration. Again, take a look at the interface:

```rust
pub trait SearchCreator {
    fn id(&self) -> &'static str;
    fn create(
        &self,
        config: &HashMap<String, String>,
    ) -> Result<Box<dyn SearchProvider>, ProviderCreationError>;
}
```

The `id` should return the `id` used to reference the profile in the configuration. The `create` should create the `SearchProvider` from the given configuration, or return a error if not enough information is given. The `create`-function should also wrap your `SearchProvider` in `SearchProviderCreator` to be able to apply standard configuration as `limit` and `show-description` without any changes to your provider. Your `SearchCreator` should also implement `From<MasterConfig>` to be easily creatable afterwards.

The last step is integrating your new provider into the application. For that, add your crate as a dependency to `mastersearch-all/Cargo.toml` and add a line to the `all-creators` function in `mastersearch-all/src/lib.rs` constructing your `SearchCreator` from the `MasterConfig`. You should also consider adding the provider as a feature in the `mastersearch-all/Cargo.toml` (and probably as a default feature) and only append your `SearchCreator` if the feature is active.

That is already everything. Do not forget to document your provider by creating a README similar to the other providers and mentioning it in the main README.
