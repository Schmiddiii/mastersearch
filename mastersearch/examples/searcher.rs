use async_trait::async_trait;
use mastersearch::{Profile, SearchProvider, SearchQuery, SearchResolver, SearchResult, Searcher};

pub struct ExampleSearchProvider(String);

#[async_trait(?Send)]
impl SearchProvider for ExampleSearchProvider {
    async fn search(
        &self,
        query: &SearchQuery,
    ) -> Result<Vec<SearchResult>, mastersearch::SearchError> {
        let string = &query.0;
        let result = (0..5)
            .map(|i| SearchResult {
                title: format!("{} {} {}", self.0, string, i),
                url: format!("url://{}/{}/{}", self.0, string, i),
                description: None,
            })
            .collect();
        Ok(result)
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let hello = ExampleSearchProvider("hello".to_string());
    let bye = ExampleSearchProvider("bye".to_string());

    let mut resolver = SearchResolver::default();
    resolver.insert("hello", hello);
    resolver.insert("bye", bye);

    let searcher = Searcher::new(resolver);
    let profile = Profile {
        providers: vec!["hello".to_string(), "bye".to_string(), "pizza".to_string()],
    };

    let results = searcher
        .search(&SearchQuery("world".to_string()), profile)
        .await;

    for result in results {
        if let Ok(searches) = result.1 {
            println!("Success: {}", result.0);
            for search in searches {
                println!("{}", search.title);
            }
        } else {
            println!("Failure: {}", result.0);
            println!("{}", result.1.err().expect("Search to be error"));
        }
    }

    Ok(())
}
