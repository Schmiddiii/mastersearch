use async_trait::async_trait;
use mastersearch::{SearchProvider, SearchQuery, SearchResult};

pub struct ExampleSearchProvider {}

#[async_trait(?Send)]
impl SearchProvider for ExampleSearchProvider {
    async fn search(
        &self,
        query: &SearchQuery,
    ) -> Result<Vec<SearchResult>, mastersearch::SearchError> {
        let string = &query.0;
        let result = (0..5)
            .map(|i| SearchResult {
                title: format!("{}{}", string, i),
                url: format!("url://{}{}", string, i),
                description: None,
            })
            .collect();
        Ok(result)
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let provider = ExampleSearchProvider {};
    for result in provider.search(&SearchQuery("hello".to_string())).await? {
        println!("Result: {} ({})", result.title, result.url);
    }
    Ok(())
}
