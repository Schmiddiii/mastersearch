# Mastersearch-Wikimedia (ID: `wikimedia`)

The mastersearch interface to Wikimedia-related websites like [wikipedia.org](https://wikipedia.org).

## Configuration

The Wikimedia-interface has one provider-specific configuration, the `url`-parameter. 
This specifies what url to take as a base for the queries. See the [MediaWiki API](https://www.mediawiki.org/wiki/API:Main_page) for examples. One example is `https://en.wikipedia.org`.
There is also a optional parameter `no-w-in-url`. This is needed on some sites that do not have a standard API path like the [Arch Wiki](https://wiki.archlinux.org/). Just leave it out for testing, if the API does not work like expected, set this to `"true"`.
