use std::collections::HashMap;

use mastersearch_wikimedia::WikimediaCreator;
use ms::{SearchCreator, SearchQuery};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = reqwest::ClientBuilder::new()
        .gzip(true)
        .user_agent("Mastersearch-Wikimedia/0.1.0 (mastersearch-wikimedia@schmiddi.anonaddy.com) reqwest/0.11")
        .build()
        .expect("Failed to build reqwest::Client");
    let mut config = HashMap::new();
    config.insert("url".to_string(), "https://en.wikipedia.org".to_string());
    let provider = WikimediaCreator { client }
        .create(&config)
        .expect("Failed to build WikimediaProvider");
    for result in provider
        .search(&SearchQuery("hello world".to_string()))
        .await?
    {
        println!("Result: {} ({})", result.title, result.url);
    }
    Ok(())
}
