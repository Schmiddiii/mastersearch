mod api;

use std::collections::HashMap;

use api::Wikimedia;
use async_trait::async_trait;
use ms::{MasterConfig, ProviderCreationError, SearchCreator, SearchProvider, SearchResult, SearchProviderWrapper};
use rrw::RestConfigBuilder;

pub struct WikimediaProvider {
    wikimedia: Wikimedia,
    url: String,
}

pub struct WikimediaCreator {
    pub client: reqwest::Client,
}

impl From<MasterConfig> for WikimediaCreator {
    fn from(c: MasterConfig) -> Self {
        Self { client: c.client }
    }
}

impl SearchCreator for WikimediaCreator {
    fn id(&self) -> &'static str {
        "wikimedia"
    }

    fn create(
        &self,
        config: &HashMap<String, String>,
    ) -> Result<Box<dyn SearchProvider>, ProviderCreationError> {
        if let Some(url) = config.get("url") {
            let full_url = if Some(&"true".to_string()) == config.get("no-w-in-url") {
                url.to_string()
            } else {
                format!("{}/w", url)
            };
            Ok(Box::new(SearchProviderWrapper::wrap(WikimediaProvider {
                wikimedia: Wikimedia::new(
                    RestConfigBuilder::new(full_url)
                        .client(self.client.clone())
                        .build(),
                ),
                url: url.to_string()
            }, config)))
        } else {
            Err(ProviderCreationError { 
                id: self.id(),
                key: "url",
                value_hint: "A value for the key 'url' has to exist describing what API to query. See 'https://www.mediawiki.org/wiki/API:Main_page'."
            })
        }
    }
}

#[async_trait(?Send)]
impl SearchProvider for WikimediaProvider {
    async fn search(
        &self,
        query: &ms::SearchQuery,
    ) -> Result<Vec<ms::SearchResult>, ms::SearchError> {
        Ok(self
            .wikimedia
            .search(&query.0)
            .await
            .map_err(rrw_to_ms_error)?
            .query
            .search
            .into_iter()
            .map(|r| SearchResult {
                title: r.title,
                url: format!("{}/?curid={}", self.url, r.pageid),
                description: Some(r.snippet),
                ..Default::default()
            })
            .collect())
    }
}

fn rrw_to_ms_error<T>(e: rrw::Error<T>) -> ms::SearchError {
    match e {
        rrw::Error::Request(e) => ms::SearchError::Request(e),
        _ => ms::SearchError::Other,
    }
}
