use serde::{Deserialize, Serialize};

#[derive(Serialize, Default, Clone)]
pub struct ApiQuery {
    pub action: String,
    pub list: String,
    pub format: String,
    pub srsearch: String,
}

#[derive(Deserialize, Default, Clone)]
pub struct Result {
    pub query: Query,
}

#[derive(Deserialize, Default, Clone)]
pub struct Query {
    pub search: Vec<Item>,
}

#[derive(Deserialize, Default, Clone)]
pub struct Item {
    pub title: String,
    pub pageid: u64,
    pub snippet: String,
}
