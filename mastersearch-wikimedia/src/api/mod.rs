mod structure;
use rrw::{Error, RestRequest, StandardRestError};
pub(crate) use structure::*;

#[derive(Clone)]
pub struct Wikimedia {
    _rest: rrw::RestConfig,
}

impl Wikimedia {
    pub fn new(rest: rrw::RestConfig) -> Self {
        Self { _rest: rest }
    }
}

impl Wikimedia {
    pub async fn search<S: AsRef<str>>(
        &self,
        query: S,
    ) -> std::result::Result<structure::Result, Error<StandardRestError>> {
        let query = ApiQuery {
            action: "query".to_string(),
            list: "search".to_string(),
            format: "json".to_string(),
            srsearch: query.as_ref().to_string(),
        };
        let request = RestRequest::<&ApiQuery, ()>::get("/api.php").query(&query);

        Ok(self._rest.execute(&request).await?)
    }
}
