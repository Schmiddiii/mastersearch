# Mastersearch-DuckDuckGo (ID: `duckduckgo`)

The mastersearch interface to DuckDuckGo search results.

## Configuration

This provider has no provider-specific configuration options.
