use ms::SearchResult;
use scraper::{Html, Selector};

pub fn extract(string: &str) -> Vec<SearchResult> {
    let html = Html::parse_document(string);

    let selector_web_result =
        Selector::parse(".web-result").expect("Failed to parse `selector_web_result`");
    let selector_title =
        Selector::parse(".result__title").expect("Failed to parse `selector_title`");
    let selector_url = Selector::parse(".result__a").expect("Failed to parse `selector_url`");
    let selector_snippet =
        Selector::parse(".result__snippet").expect("Failed to parse `selector_snippet`");

    html.select(&selector_web_result)
        .map(|web_result| SearchResult {
            title: web_result
                .select(&selector_title)
                .map(|title| title.text().collect::<String>().trim().to_owned())
                .next()
                .unwrap_or_else(|| "".to_string()),
            url: web_result
                .select(&selector_url)
                .map(|url| convert_url(url.value().attr("href").unwrap_or("")))
                .next()
                .unwrap_or_else(|| "".to_string()),
            description: web_result
                .select(&selector_snippet)
                .map(|description| description.text().collect::<String>().trim().to_owned())
                .next(),
            ..Default::default()
        })
        .collect()
}

fn convert_url(url: &str) -> String {
    let without_prefix = url.strip_prefix("//duckduckgo.com/l/?uddg=").unwrap_or(url);
    let without_postfix = without_prefix
        .rsplit_once("&rut=")
        .map(|(p1, _p2)| p1)
        .unwrap_or(without_prefix);
    urlencoding::decode(without_postfix)
        .map(|c| c.into_owned())
        .unwrap_or(without_prefix.to_owned())
}
