mod scraper;

use std::collections::HashMap;

use async_trait::async_trait;
use ms::{
    MasterConfig, ProviderCreationError, SearchCreator, SearchProvider, SearchProviderWrapper,
};

pub struct DuckDuckGoCreator {
    pub client: reqwest::Client,
}

impl From<MasterConfig> for DuckDuckGoCreator {
    fn from(c: MasterConfig) -> Self {
        Self { client: c.client }
    }
}

pub struct DuckDuckGoProvider {
    client: reqwest::Client,
}

impl SearchCreator for DuckDuckGoCreator {
    fn id(&self) -> &'static str {
        "duckduckgo"
    }

    fn create(
        &self,
        config: &HashMap<String, String>,
    ) -> Result<Box<dyn SearchProvider>, ProviderCreationError> {
        Ok(Box::new(SearchProviderWrapper::wrap(
            DuckDuckGoProvider {
                client: self.client.clone(),
            },
            config,
        )))
    }
}

#[async_trait(?Send)]
impl SearchProvider for DuckDuckGoProvider {
    async fn search(
        &self,
        query: &ms::SearchQuery,
    ) -> Result<Vec<ms::SearchResult>, ms::SearchError> {
        let text = self
            .client
            .get(format!("https://html.duckduckgo.com/html&q={}", query.0))
            .send()
            .await?
            .text()
            .await?;
        Ok(scraper::extract(&text))
    }
}
